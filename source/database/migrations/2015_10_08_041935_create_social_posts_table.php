<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('social_posts', function (Blueprint $table) {
            $table->increments('id');

            // Post Information
            $table->string('network', 50);
            $table->string('network_id', 200);
            $table->text('url');
            $table->enum('media_type', ['image', 'video']);
            $table->text('image')->nullable();
            $table->text('image_thumb')->nullable();
            $table->text('video')->nullable();
            $table->string('title')->nullable();
            $table->text('comments');
            $table->string('author_name');
            $table->text('author_image');
            $table->string('author_username');
            $table->string('author_profile');
            $table->dateTime('post_date');

            // Moderation & Approval Fields
            $table->boolean('reviewed')->default(false);
            $table->boolean('approved')->default(false);
            $table->boolean('valid_entry')->default(false);
            $table->text('errors');

            // Unique Constraint
            $table->unique(['network', 'network_id']);

            // Timestamps
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('social_posts');
    }
}
