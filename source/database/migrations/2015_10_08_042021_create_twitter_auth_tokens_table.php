<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitterAuthTokensTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twitter_auth_tokens', function(Blueprint $table)
        {
            // Define Properties.
            $table->engine = 'InnoDB';

            // Define Columns
            $table->increments('id')->unsigned();
            $table->timestamps();
            $table->string('token', 255);

            // Define Indexes
            $table->index('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('twitter_auth_tokens');
    }

}
