<?php namespace Brandmovers\Social\Twitter;

use Illuminate\Database\Eloquent\Model;
use Brandmovers\Promotion\Period;
use Carbon\Carbon;


class OAuth extends Model {

	protected $table = "twitter_oauth";
	
	/**
	 * Get the latest set of OAuth credentials
	 */
	public static function getLatest() {
		return self::orderBy('created_at', 'desc')->first();
	}
	
}
