<?php

namespace Brandmovers\Social\TintUp;


use Illuminate\Support\Facades\Config;
use Brandmovers\Social\Post;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Carbon\Carbon;



class TintUpImporter
{
    const MAX_REQUESTS = 20;

    private $recordsImported = 0;
    private $maxTimestamp = null;
    private $apiRequests = 0;


    /**
     * Determine the initial URL for the feed based on configuration parameters
     *
     * @return string
     */
    private function getFeedUrl() {
        return rtrim(Config::get('tintup.url'), '/') . '/' . Config::get('tintup.tint_name');
    }


    /**
     * Query the TintUp API and return a Guzzle Response Stream
     *
     * @param null $url
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    private function queryApi($url = null) {
        $client = new GuzzleClient();

        if( $url ) {
            return $client->request('GET', $url);
        }
        else {
            return $client->request('GET', $this->getFeedUrl(), [
                'query' => [
                    'api_token' => Config::get('tintup.api_token'),
                    'source'    => 'instagram'  // Instagram ONLY!!  Twitter direct API integration running separately
                ]
            ]);
        }
    }


    /**
     * Run the import process
     *
     * @param null $url
     */
    public function import($url = null) {

        $finished = false;
        $records = 0;
        $maxRequests = self::MAX_REQUESTS;

        // Check for the latest record we've imported
        $this->maxTimestamp = TintUpRequestLog::getMaxTimestamp();
        $log = new TintUpRequestLog();


        // Only run one request for initial run
        if( $this->maxTimestamp == null ) {
            $maxRequests = 1;
        }


        // Query the API
        $response = $this->queryApi($url);
        $this->apiRequests++;


        if( $response->getStatusCode() == 200 ) {

            $json = @json_decode($response->getBody()->getContents(), false, 512, JSON_BIGINT_AS_STRING);
            $runMaxTimestamp = null;

//            dd($json);
//            //has next page data
            do{
                foreach($json->data as $data) {
                    // Check if we've already imported beyond this point
                    if( $data->created < $this->maxTimestamp ) {
                        $finished = true;
                        break;
                    }

                    // Assign max timestamp
                    if( $runMaxTimestamp == null || $data->created > $runMaxTimestamp ) {
                        $runMaxTimestamp = $data->created;
                    }

                    // Save as Social Post
                    $saved = $this->convertToPost($data);

                    if( $saved ) {
                        $records++;
                    }
                }

                $log->max_timestamp = $runMaxTimestamp;
                $log->records = $records;

                if( $finished ) {
                    // Import Completed
                    $log->status = "COMPLETED";
                    break;
                }
                else {
                    // Get Next Page
                    if( $this->apiRequests < $maxRequests ) {
                        $log->status = "PAGED";
                        $this->import($json->next_page);
                    }
                    else {
                        // Max Requests Reached
                        $log->status = "COMPLETED";
                        $log->message = "Max requests per run reached";
                    }
                }
                $searchNextPage = $json->has_next_page;

                if($searchNextPage){
                    $nextPageUrl = $json->next_page;
                    try {
                        $json = json_decode(file_get_contents($nextPageUrl, false));
                        if(!isset($json->data)){
                            $searchNextPage = false;
                            $log->status = "FAILED";
                            $log->message = 'Error occured while getting next page content!';
                        }
                    }
                    catch(\Exception $e) {
                        $searchNextPage = false;
                        $log->status = "FAILED";
                        $log->message = 'Error occured while getting next page content!';
                        echo 'Error occured while getting next page content!';
                    }
                }
            }while($searchNextPage);

        }
        else {
            $log->status = "FAILED";
        }
        $log->request_date = Carbon::now();
        $log->save();
    }


    /**
     * Convert a TintUp data object into Social Post
     *
     * @param $data
     */
    public function convertToPost($data) {

        // Encode nested properties in data.  Not sure why these don't get decoded in the original decode...
        $data->author = json_decode($data->author);
        $data->embed = json_decode($data->embed);

        $post = new Post([
            'network'           => $data->social_id,
            'network_id'        => $data->social_post_id,
            'url'               => $data->url,
            'media_type'        => 'image', // Not available from Tint
            'image'             => $data->original_image,
            'image_thumb'       => $data->image,
            'video'             => null,
            'title'             => null,
            'comments'          => $data->comments,
            'author_name'       => $data->author->name,
            'author_image'      => $data->author->picture,
            'author_username'   => $data->author->username,
            'author_profile'    => $data->author->link,
            'post_date'         => Carbon::createFromTimestamp($data->created)
        ]);

        // Validate this post
        $post->validate();

        // Save the tweet.
        return $post->saveIgnoreDuplicates();

    }

}