<?php namespace Brandmovers\Social\Instagram;

use Illuminate\Support\Facades\Config;
use Brandmovers\Social\Instagram\InstagramRequestLog;
use Instagram\Instagram;
use Brandmovers\Social\Post;

class InstagramImporter
{
	
	public function import() {
		
		// Create a new request log
		$lastId = InstagramRequestLog::getLastId();

		// Account for Instagram changing tag min/max from integer based Media IDs to special tag alphanumerics
		if( is_numeric($lastId) ) {
			$lastId = null;
		}

		// Run initial import
		$results = $this->getMedia($lastId, null);

		if( $lastId ) {
			// Check if we need query again for more posts (page through results until we get to our last import)
			while( $results && $results->getNextMaxTagId() ) {
				// Get the next page of data (still above the original min, but after the last page)
				$results = $this->getMedia($lastId, $results->getNextMaxTagId());
			}
		}
		
	}



	public function getMedia($minId, $maxId) {
		
		echo "\n";
		echo "---------------------------------------------------------------\n";
		echo "Starting API Request\n";
		echo "---------------------------------------------------------------\n";

		// Create new Request Log (sets created_at and status of RUNNING as defaults)
		$requestLog = InstagramRequestLog::create([]);
		
		
		// Setup Instagram Client
		$instagram = new Instagram();
		$instagram->setClientID(Config::get('instagram.client_id'));
		$tag = $instagram->getTag(Config::get('instagram.tag'));


		// Get Media from Instagram Tag within the ID ranges
		$media = $tag->getMedia([
			'min_tag_id' => $minId,
			'max_tag_id' => $maxId
		]);


		// Record number of records found
		$requestLog->records = count($media);


		// Check for results		
		if( count($media) == 0 ) {
			echo "No new media found, saving log and exiting\n\n";
			$requestLog->status = InstagramRequestLog::STATUS_COMPLETE;
			$requestLog->save();
			return;
		}
		
		
		// Save each post to the database
		foreach($media as $gram) {

			$mediaType = ($gram->type == "video") ? Post::MEDIA_TYPE_VIDEO : Post::MEDIA_TYPE_IMAGE;

			// Sometimes the Instagram API reports the caption as null, therefor there is no text.
			// Set it here to an empty string to avoid errors below
			if( !isset($gram->caption->text) ) {
				$comments = "";
			}
			else {
				$comments = $gram->caption->text;
			}


			// Check for video
			if( $gram->type == 'video' ) {
				if( !isset($gram->videos->standard_resolution->url) ) {
					$video = null;
				}
				else {
					$video = $gram->videos->standard_resolution->url;
				}
			}
			else {
				$video = null;
			}

			
			$post = new Post([
				'network'			=> Post::NETWORK_INSTAGRAM,
				'network_id'		=> $gram->id,
				'post_date' 		=> date('Y-m-d H:i:s', $gram->created_time),
				'url'				=> $gram->link,
				'media_type'		=> $mediaType,
				'image'				=> $gram->images->standard_resolution->url,
				'image_thumb'		=> $gram->images->thumbnail->url,
				'video'				=> $video,
				'comments'			=> $comments,
				'author_name'		=> $gram->user->full_name,
				'author_image'		=> $gram->user->profile_picture,
				'author_username'	=> $gram->user->username,
				'author_profile'	=> "http://instagram.com/" . urlencode($gram->user->username)
			]);


			// Validate this post
			$post->validate();
			
			try {
				$post->save();
				echo "Saved post $gram->id to DB\n";
			}
			catch(\Exception $e) {
				// Catch duplicate entry constraint and ignore
				if( $e->errorInfo[0] == "23000" ) {
					echo "Error inserting duplicate with gram $gram->id\n";
				}
				else {
					// Mark import as errored					
					throw $e;
				}
			}
			
		}
		
		
		echo "*** COMPLETE ***\n";
		echo "Imported " . count($media) . " records\n\n";
		
		
		// Complete the Request Log
		$requestLog->status = InstagramRequestLog::STATUS_COMPLETE;
		$requestLog->min_tag_id = $media->getMinTagId();
		$requestLog->max_tag_id = $media->getNextMaxTagId();
		$requestLog->save();
		
		return $media;
		
	}



	public function fillGap() {

		// Min ID of where to start -- Where the new import process picked up (min ID from first successful import)
		$minTagId = "AQDe-PMlwgjn04MdyNQKErhqjmJ0-CUVGg1xBpWdWp4ERu7QSD91Sz-sfAAwm-HjRlNODVMfac1jktzSzrykS_5AgpjjZIwhr0dAmo3cOyjTPOcSqeHOsfL8Z0wDv-ltsEdGrL6EBcW0iWASpEJ-RKSyu6-2Q9s9HFC1mbNHfOuqgw";


		if( $lastId ) {
			// Check if we need query again for more posts (page through results until we get to our last import)
			while( $results && $results->getNextMaxTagId() ) {
				// Get the next page of data (still above the original min, but after the last page)
				$results = $this->getMedia($lastId, $results->getNextMaxTagId());
			}
		}



	}



	
}
	