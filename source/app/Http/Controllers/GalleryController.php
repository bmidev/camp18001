<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Brandmovers\Social\Post;
use Carbon\Carbon;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $posts = Post::gallery();

        // Paging by MaxTime
        if( $request->input('maxTime') ) {
            $maxTime = new Carbon($request->input('maxTime'));
            $posts = $posts->where('post_date', '<', $maxTime);
        }

        // Page Size (max 100 at a time)
        $pageSize = $request->input('pageSize', 8);
        $pageSize = ($pageSize > 100) ? 100 : $pageSize;
        $posts = $posts->limit($pageSize);

        // Filters
        if($request->input('filter')){
            $timeline = \DB::table('week_timelines')->where('id', $request->input('filter'))->first();
            $posts = $posts->where('post_date', '>', $timeline->start)->where('post_date', '<', $timeline->end);
        }

        // Filters
//        switch($request->input('filter')) {
//            case 'video':
//                $posts = $posts->where('media_type', Post::MEDIA_TYPE_VIDEO);
//                break;
//            case 'photo':
//                $posts = $posts->where('media_type', Post::MEDIA_TYPE_IMAGE);
//                break;
//            case 'winners':
//                $posts = $posts->where(function($query) {
//                    $query->where('contest_winner', true)->orWhere('sweepstakes_winner', true);
//                });
//                break;
//        }

        // Query
        $posts = $posts->get();

        // Convert to public facing JSON array
        $postData = [];
        foreach($posts as $post) {
            $postData[] = $post->toPublicJson();
        }

        // Get last timestamp from results (for requesting next page)
        $lastItem = $posts->last();
        $maxTime = $lastItem ? $lastItem->post_date : null;

        return response()->json([
            'posts' => $postData,
            'maxTime' => $maxTime
        ]);

    }

}
