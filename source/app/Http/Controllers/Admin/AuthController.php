<?php namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class AuthController extends Controller {

	use AuthenticatesUsers, ThrottlesLogins;

	protected $loginPath = "/admin";
	protected $lockoutTime = 900; // == 60 * 15 == 15 Minutes
	protected $maxLoginAttempts = 10; // 10 failed attempts

	/*
	|---------------------------------------------------------------------------
	| Admin Dashboard Controller
	|---------------------------------------------------------------------------
	|
	| Primary admin controller displaying dashboard stats
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Admin Login
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		if( !Auth::guest() ) {
			return redirect('/admin/dashboard');
		}
		
		return view('admin.login', []);
	}


	/**
	 * Process Admin Login
	 * 
	 * @param Request $request
	 * @return Response
	 */
	public function postIndex(Request $request)
	{
		$email = $request->input('email');
		$password = $request->input('password');

		if ($this->hasTooManyLoginAttempts($request)) {
			return $this->sendLockoutResponse($request);
		}

		if( Auth::attempt(['email' => $email, 'password' => $password])) {
			return redirect()->intended('/admin/dashboard');
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return redirect()
			->back()
			->withInput()
			->withErrors('Invalid Login.');
		
	}


 	/**
	 * Logout of Admin
	 */
    public function getLogout()
    {
        Auth::logout();
        return redirect('/admin');
    }

	
}

