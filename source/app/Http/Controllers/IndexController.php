<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Brandmovers\Social\Post;
use Carbon\Carbon;

class IndexController extends Controller
{
    private $promo_over;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $date = Carbon::now();

        // Promo Over Check
        $this->promo_over = $date->toDateTimeString() > config('app.promo_over') ? true : false;
        
        $dateOverride = $request->input('date', null);
        $dateParts = explode("-", $dateOverride);

        if( count($dateParts) == 2 ) {
            $date->day = 1;
            $date->year = $dateParts[0];
            $date->month = $dateParts[1];
        }

        $month = sprintf("%02d", $date->month);
        $year = $date->year;
        
        $weeks = \DB::table('week_timelines')->where('start', '<', Carbon::now())->get();

        return view('index', [
            'month' => $month,
            'year'  => $year,
            'dateString' => $year . "_" . $month,
            'weeks' => $weeks,
            'promo_over' => $this->promo_over 
        ]); 
    }


    public function rules(Request $request) {
        $date = Carbon::now();

        $dateOverride = $request->input('date', null);
        $dateParts = explode("-", $dateOverride);

        if( count($dateParts) == 2 ) {
            $date->year = $dateParts[0];
            $date->month = $dateParts[1];
        }

        $month = sprintf("%02d", $date->month);
        $year = $date->year;

        return view('rules', [
            'month' => $month,
            'year'  => $year,
            'dateString' => $year . "_" . $month
        ]);
    }


}