app.directive('videoResize', ['$window', '$rootScope', function($window, $rootScope) {
    return {
        restrict: 'A',
        link: function(scope,element,attrs) {

            function adjustHeight() {
                element.height(element.width());
            }

            adjustHeight();

            angular.element($window).bind('resize', adjustHeight);
            $rootScope.$on('video-resize', adjustHeight);

        }
    }
}]);