app.directive('socialGallery',['$timeout', function($timeout){
    var linker = function(scope,element,attrs) {
        var $grid;

        var setGrid = function() {
            $grid = $(element).isotope({
                itemSelector: '.mix',
                percentPosition: true,
                masonry: {
                    // use outer width of grid-sizer for columnWidth
                    columnWidth: '.grid-sizer'
                }
            });
        };

        var updateGrid = function() {
            var imgLoad = imagesLoaded($(element));

            imgLoad.on('progress', function(instance, img) {
                $grid.isotope('reloadItems').isotope();
            }).on('done', function() {
                // (This doesn't belong here, but reset parallax after images loaded)
                $(window).data('plugin_stellar').destroy();
                $(window).data('plugin_stellar').init();
            });
        };

        setGrid();


        // Update gallery and reload items when the backing entities are updated
        scope.$watch('entities', function() {
            $timeout(function() {
                if( $grid ) {
                    updateGrid();
                }
            });
        }, true);


    };
    return {
        restrict:'A',
        link: linker,
        scope:{
            entities:'='
        }
    }
}]);
