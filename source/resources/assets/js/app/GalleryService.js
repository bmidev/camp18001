app.factory('Gallery', ['$resource', function($resource) {
    return $resource(BASE_URL + '/gallery', null, {
        page: {method: 'GET',url: '/gallery',isArray: false }
    });
}]);
