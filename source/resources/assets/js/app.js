/*! modernizr 3.1.0 (Custom Build) | MIT *
 * http://modernizr.com/download/?-flexbox-flexboxlegacy-flexboxtweener !*/
!function(e,n,t){function r(e,n){return typeof e===n}function o(){var e,n,t,o,s,i,a;for(var l in x){if(e=[],n=x[l],n.name&&(e.push(n.name.toLowerCase()),n.options&&n.options.aliases&&n.options.aliases.length))for(t=0;t<n.options.aliases.length;t++)e.push(n.options.aliases[t].toLowerCase());for(o=r(n.fn,"function")?n.fn():n.fn,s=0;s<e.length;s++)i=e[s],a=i.split("."),1===a.length?Modernizr[a[0]]=o:(!Modernizr[a[0]]||Modernizr[a[0]]instanceof Boolean||(Modernizr[a[0]]=new Boolean(Modernizr[a[0]])),Modernizr[a[0]][a[1]]=o),g.push((o?"":"no-")+a.join("-"))}}function s(e){var n=w.className,t=Modernizr._config.classPrefix||"";if(_&&(n=n.baseVal),Modernizr._config.enableJSClass){var r=new RegExp("(^|\\s)"+t+"no-js(\\s|$)");n=n.replace(r,"$1"+t+"js$2")}Modernizr._config.enableClasses&&(n+=" "+t+e.join(" "+t),_?w.className.baseVal=n:w.className=n)}function i(e,n){return function(){return e.apply(n,arguments)}}function a(e,n,t){var o;for(var s in e)if(e[s]in n)return t===!1?e[s]:(o=n[e[s]],r(o,"function")?i(o,t||n):o);return!1}function l(e,n){return!!~(""+e).indexOf(n)}function f(){return"function"!=typeof n.createElement?n.createElement(arguments[0]):_?n.createElementNS.call(n,"http://www.w3.org/2000/svg",arguments[0]):n.createElement.apply(n,arguments)}function u(e){return e.replace(/([a-z])-([a-z])/g,function(e,n,t){return n+t.toUpperCase()}).replace(/^-/,"")}function d(e){return e.replace(/([A-Z])/g,function(e,n){return"-"+n.toLowerCase()}).replace(/^ms-/,"-ms-")}function c(){var e=n.body;return e||(e=f(_?"svg":"body"),e.fake=!0),e}function p(e,t,r,o){var s,i,a,l,u="modernizr",d=f("div"),p=c();if(parseInt(r,10))for(;r--;)a=f("div"),a.id=o?o[r]:u+(r+1),d.appendChild(a);return s=f("style"),s.type="text/css",s.id="s"+u,(p.fake?p:d).appendChild(s),p.appendChild(d),s.styleSheet?s.styleSheet.cssText=e:s.appendChild(n.createTextNode(e)),d.id=u,p.fake&&(p.style.background="",p.style.overflow="hidden",l=w.style.overflow,w.style.overflow="hidden",w.appendChild(p)),i=t(d,e),p.fake?(p.parentNode.removeChild(p),w.style.overflow=l,w.offsetHeight):d.parentNode.removeChild(d),!!i}function m(n,r){var o=n.length;if("CSS"in e&&"supports"in e.CSS){for(;o--;)if(e.CSS.supports(d(n[o]),r))return!0;return!1}if("CSSSupportsRule"in e){for(var s=[];o--;)s.push("("+d(n[o])+":"+r+")");return s=s.join(" or "),p("@supports ("+s+") { #modernizr { position: absolute; } }",function(e){return"absolute"==getComputedStyle(e,null).position})}return t}function v(e,n,o,s){function i(){d&&(delete z.style,delete z.modElem)}if(s=r(s,"undefined")?!1:s,!r(o,"undefined")){var a=m(e,o);if(!r(a,"undefined"))return a}for(var d,c,p,v,h,y=["modernizr","tspan"];!z.style;)d=!0,z.modElem=f(y.shift()),z.style=z.modElem.style;for(p=e.length,c=0;p>c;c++)if(v=e[c],h=z.style[v],l(v,"-")&&(v=u(v)),z.style[v]!==t){if(s||r(o,"undefined"))return i(),"pfx"==n?v:!0;try{z.style[v]=o}catch(g){}if(z.style[v]!=h)return i(),"pfx"==n?v:!0}return i(),!1}function h(e,n,t,o,s){var i=e.charAt(0).toUpperCase()+e.slice(1),l=(e+" "+S.join(i+" ")+i).split(" ");return r(n,"string")||r(n,"undefined")?v(l,n,o,s):(l=(e+" "+E.join(i+" ")+i).split(" "),a(l,n,t))}function y(e,n,r){return h(e,t,t,n,r)}var g=[],x=[],C={_version:"3.1.0",_config:{classPrefix:"",enableClasses:!0,enableJSClass:!0,usePrefixes:!0},_q:[],on:function(e,n){var t=this;setTimeout(function(){n(t[e])},0)},addTest:function(e,n,t){x.push({name:e,fn:n,options:t})},addAsyncTest:function(e){x.push({name:null,fn:e})}},Modernizr=function(){};Modernizr.prototype=C,Modernizr=new Modernizr;var w=n.documentElement,_="svg"===w.nodeName.toLowerCase(),b="Moz O ms Webkit",S=C._config.usePrefixes?b.split(" "):[];C._cssomPrefixes=S;var E=C._config.usePrefixes?b.toLowerCase().split(" "):[];C._domPrefixes=E;var T={elem:f("modernizr")};Modernizr._q.push(function(){delete T.elem});var z={style:T.elem.style};Modernizr._q.unshift(function(){delete z.style}),C.testAllProps=h,C.testAllProps=y,Modernizr.addTest("flexbox",y("flexBasis","1px",!0)),Modernizr.addTest("flexboxlegacy",y("boxDirection","reverse",!0)),Modernizr.addTest("flexboxtweener",y("flexAlign","end",!0)),o(),s(g),delete C.addTest,delete C.addAsyncTest;for(var N=0;N<Modernizr._q.length;N++)Modernizr._q[N]();e.Modernizr=Modernizr}(window,document);

// Application Javascript

$(document).ready(function () {
    $('.menu').data('size','big');
    $('.menu nav').show();

    $(window).stellar({
        hideDistantElements: false
    });

    //$.scrollUp({
    //    scrollText:'',
    //    scrollDistance: 700
    //});

    $(document).on("scroll", onScroll);
    $('.mobile-menu a').on('click', function(){
        $('.menu .mobile-menu').toggleClass('open');
    });
    
    //smoothscroll
    $('a[href*=#]:not([href=#])').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");
        
        $('a').each(function () {
            $(this).removeClass('active');
        })
        $(this).addClass('active');
      
        var target = this.hash,
            menu = target;
        $target = $(target);
        $('html, body').stop().animate({
            'scrollTop': $target.offset().top
        }, 500, 'swing', function () {
            // window.location.hash = target;
            $(document).on("scroll", onScroll);
        });
    });


    // Load YouTube API Async
    var tag = document.createElement('script');
    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // TODO Replace with bootstrap modal, remove magnificPopup dependency
    //$('a.rules-popup').magnificPopup({
    //    type: 'ajax',
    //    overflowY: 'scroll' // as we know that popup content is tall we set scroll overflow by default to avoid jump
    //});


    $('.share-twitter').click(function(e) {
        e.preventDefault();
        //var loc = $(this).attr('href');
        var loc = 'http://bit.ly/2o6DKo6';
        var text = "Pepperidge Farm wants to see your buns (and burgers). Post a pic for a chance to win $10,000! Learn more";
        return openShare('http://twitter.com/share?url=' + loc + '&text=' + encodeURIComponent(text) + '&', 'twitterwindow');
    });
    $('.share-facebook').click(function(e) { 
        e.preventDefault(); 
        return openShare('http://www.facebook.com/sharer.php?u=' + window.location.href);
    });
    function openShare(url) {
        window.open(url, 'shareWindow', 'height=450, width=550, top='+($(window).height()/2 - 225) +', left='+$(window).width()/2 +', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');
    }

});

function onScroll(event){
    var scrollPos = $(document).scrollTop();
    if($(document).scrollTop() > 0)
    {
        if($('.menu').data('size') == 'big')
        {
            $('.menu').data('size','small');
            $('nav .logo-wrapper img').stop().animate({
                width: '70%'
            }, 600);
            if($(document).innerWidth() < 630){
                $('.menu nav .ext-pad').stop().animate({
                    padding: '5px 0 55px'
                }, 900);
            }else{
                $('.menu nav').stop().animate({
                    marginTop: '23'
                }, 600);
            }
        }
    }
    else
    {
        if($('.menu').data('size') == 'small')
        {
            $('.menu').data('size','big');
            $('nav .logo-wrapper img').stop().animate({
                width: '100%'
            }, 600);
            if($(document).innerWidth() < 630){
                $('.menu nav .ext-pad').stop().animate({
                    padding: '5px 0 8px'
                }, 600);
            }else{
                $('.menu nav').stop().animate({
                    marginTop: '40'
                }, 600);
            }
        }
    }
    $('nav a[href*=#]:not([href=#])').each(function () {
        var currLink = $(this);
        var refElement = $(currLink.attr("href"));
        if (refElement.position()) {
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() -63 > scrollPos) {
                $('nav a').removeClass("active");
                currLink.addClass("active");
            }
            else{
                currLink.removeClass("active");
            }
        }
    });
}


var app = angular.module('app', [
        'ngSanitize',
        'yaru22.angular-timeago',
        'matchMedia',
        'ngResource',
        'ui.bootstrap',
        'com.2fdevs.videogular',
        'com.2fdevs.videogular.plugins.controls',
        'slick'
    ], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
    }
);

app.filter('nl2br', ['$sce', function($sce){
    return function(msg,is_xhtml) {
        var is_xhtml = is_xhtml || true;
        var breakTag = (is_xhtml) ? '<br />' : '<br>';
        var msg = (msg + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
        return $sce.trustAsHtml(msg);
    }
}]);

