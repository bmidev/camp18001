<div class="official-rules"> <span title="Close (Esc)" class="modal-close" ng-click="$dismiss()"></span>
  <div class="tac">RULES</div>
  <h2>Pepperidge Farm Respect the Bun Promotion</h2>
  <br/>
  <p><strong>NO PURCHASE NECESSARY. A PURCHASE WILL NOT IMPROVE YOUR CHANCE OF WINNING.  VOID WHERE PROHIBITED.</strong><br>
    <strong> </strong><br>
    <strong>BY ENTERING THE PROMOTION, ENTRANT AGREES  TO THESE OFFICIAL RULES.</strong></p>
  <p><strong>ALL PHOTOS SUBMITTED MAY APPEAR ON  SPONSOR&rsquo;S WEBSITE, OR SPONSOR&rsquo;S INSTAGRAM AND TWITTER PAGES, OR IN ANY OTHER  MEDIUM FOR ANY PURPOSE WITHOUT NOTICE, APPROVAL, ATTRIBUTION, OR ADDITIONAL  COMPENSATION IN SPONSOR&rsquo;S SOLE DISCRETION.</strong>  <strong></strong></p>
  <ol>
    <li><strong>Eligibility: </strong>The Pepperidge Farm<sup>&reg;</sup> Respect the Bun Promotion (the &ldquo;Promotion&rdquo;) consists of two (2) parts: (i) a sweepstakes portion of the Promotion (the &ldquo;Sweepstakes Portion&rdquo;); and (ii) a contest portion of the Promotion (&ldquo;Contest Portion&rdquo;). The Promotion is open only to legal residents of the 50 United States and District of Columbia, who are 18 years of age (or 19 for residents of AL and NE) or older at the time of entry. Void where prohibited by law. No purchase necessary to enter the Promotion.&nbsp;Employees of Pepperidge Farm, Incorporated, 595 Westport Ave, Norwalk CT 06851 (the &ldquo;Sponsor&rdquo;), Brandmovers, Inc., 590 Means Street, Suite 250, Atlanta, GA 30318. (&ldquo;Administrator&rdquo;), Campbell&rsquo;s Soup Company, or any of their respective affiliates, subsidiaries, advertising agencies, or any other company or individual involved with the design, production, execution or distribution of the Promotion (collectively with Sponsor and Administrator, the "Promotion Entities") and their immediate family (spouse, parents and step-parents, siblings and step- siblings, and children and step-children) and household members of each such employee are not eligible to enter or win. &nbsp;The Promotion is subject to all applicable federal, state, and local laws and regulations. &nbsp;Participation constitutes entrant's full and unconditional agreement to these Official Rules and Sponsor's decisions, which are final and binding in all matters related to the Promotion. &nbsp;Winning a prize is contingent upon fulfilling all requirements set forth herein.</li>
    <li><strong>Promotion Period:  </strong>The Promotion Period begins on May 14, 2018 at 12:00:00 PM Eastern  Time (&ldquo;ET&rdquo;) and ends on September 4, 2018 at 11:59:59 AM ET (the &quot;Promotion  Period&quot;). The Sweepstakes Portion of the Promotion will be broken out into  sixteen (16) weekly sweepstakes, starting on Monday and ending the following Monday  of each week throughout the Promotion Period, with the exception of the sixteenth  and final week, which will end on Tuesday, September 5, 2017 (each, a &ldquo;Weekly Sweepstakes  Period&rdquo;) per the Weekly Sweepstakes Periods chart below. The Contest Period and  total Weekly Sweepstakes Periods will run concurrently with the Promotion  Period.</li>
  </ol>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td width="590" colspan="5" valign="top"><p align="center"><strong>WEEKLY SWEEPSTAKES    PERIODS</strong></p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">Weekly Sweepstakes Period #</p></td>
      <td width="123"><p align="center">Start Date @ 12:00:00 PM ET on:</p></td>
      <td width="123"><p align="center">End Date @ 11:59:59 AM ET on:</p></td>
      <td width="125"><p align="center">&nbsp;</p>
        <p align="center">Random Drawing:</p></td>
      <td width="103"><p align="center">Prize:</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">1</p></td>
      <td width="123"><p align="center">5/14/18</p></td>
      <td width="123"><p align="center">5/21/18</p></td>
      <td width="125"><p align="center">5/24/18</p></td>
      <td width="103"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">2</p></td>
      <td width="123"><p align="center">5/21/18</p></td>
      <td width="123"><p align="center">5/28/18</p></td>
      <td width="125" valign="top"><p align="center">5/30/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">3</p></td>
      <td width="123"><p align="center">5/28/18</p></td>
      <td width="123"><p align="center">6/4/18</p></td>
      <td width="125" valign="top"><p align="center">6/6/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">4</p></td>
      <td width="123"><p align="center">6/4/18</p></td>
      <td width="123"><p align="center">6/11/18</p></td>
      <td width="125" valign="top"><p align="center">6/13/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">5</p></td>
      <td width="123"><p align="center">6/11/18</p></td>
      <td width="123"><p align="center">6/18/18</p></td>
      <td width="125" valign="top"><p align="center">6/20/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">6</p></td>
      <td width="123"><p align="center">6/18/18</p></td>
      <td width="123"><p align="center">6/25/18</p></td>
      <td width="125" valign="top"><p align="center">6/27/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">7</p></td>
      <td width="123"><p align="center">6/25/18</p></td>
      <td width="123"><p align="center">7/2/18</p></td>
      <td width="125" valign="top"><p align="center">7/5/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">8</p></td>
      <td width="123"><p align="center">7/2/18</p></td>
      <td width="123"><p align="center">7/09/18</p></td>
      <td width="125" valign="top"><p align="center">7/11/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">9</p></td>
      <td width="123"><p align="center">7/09/18</p></td>
      <td width="123"><p align="center">7/16/18</p></td>
      <td width="125" valign="top"><p align="center">7/18/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">10</p></td>
      <td width="123"><p align="center">7/16/18</p></td>
      <td width="123"><p align="center">7/23/18</p></td>
      <td width="125" valign="top"><p align="center">7/25/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">11</p></td>
      <td width="123"><p align="center">7/23/18</p></td>
      <td width="123"><p align="center">7/30/18</p></td>
      <td width="125" valign="top"><p align="center">8/1/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">12</p></td>
      <td width="123"><p align="center">7/30/18</p></td>
      <td width="123"><p align="center">8/6/18</p></td>
      <td width="125" valign="top"><p align="center">8/8/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">13</p></td>
      <td width="123"><p align="center">8/6/18</p></td>
      <td width="123"><p align="center">8/13/18</p></td>
      <td width="125" valign="top"><p align="center">8/15/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">14</p></td>
      <td width="123"><p align="center">8/13/18</p></td>
      <td width="123"><p align="center">8/20/18</p></td>
      <td width="125" valign="top"><p align="center">8/22/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">15</p></td>
      <td width="123"><p align="center">8/20/18</p></td>
      <td width="123"><p align="center">8/27/18</p></td>
      <td width="125" valign="top"><p align="center">8/29/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
    <tr>
      <td width="117"><p align="center">16</p></td>
      <td width="123"><p align="center">8/27/18</p></td>
      <td width="123"><p align="center">9/4/18</p></td>
      <td width="125" valign="top"><p align="center">9/6/18</p></td>
      <td width="103" valign="top"><p align="center">$1,000.00</p></td>
    </tr>
  </table>
  <p align="center">&nbsp;</p>
  <p>The  Website (defined below) clock is the official timekeeper for this Promotion. </p>
  <ol start="3">
    <li><strong>How to Enter: </strong>
      <ol type="a">
        <li>Social Media Entry with  Photo: To enter the Sweepstakes and Contest Portions of the Promotion, during the Promotion Period, upload an original Photo of your hot dog and/or burger with a Pepperidge Farm&reg; bun (the Photo does not have to include the packaging, it can be any original photo of the Pepperidge Farm bun with hot dog and/or burger) (&ldquo;Photo&rdquo;) to his or her own publically available Instagram or Twitter page, using the hashtag <strong>#RespectTheBunPromotion</strong> to receive one (1) entry into the Contest Portion of the Promotion, and one (1) simultaneous entry into the Sweepstakes Portion of the Promotion for that week&rsquo;s Weekly Sweepstakes Period.<strong> &nbsp;</strong>All Photos must be received by 11:59:59 AM ET on End Date listed in the Weekly Sweepstakes Periods Chart in Official Rule #2 (&ldquo;Chart&rdquo;) above to be eligible for a particular Weekly Sweepstakes Period, or by 11:59:59 AM on September 5, 2017 for the final Weekly Sweepstakes Period, and to be entered into the Contest Portion of the Promotion.</li>
        <li>Alternate Method of Entry (AMOE) with Photo: To enter the Sweepstakes and Contest Portions of the Promotion without using a Pepperidge Farm&reg; bun, take a Photo of your hot dog and/or burger with any bun (does NOT have to be a Pepperidge Farm brand) or any other way one may wrap or serve a hot dog and/or burger, even without a bun, and email the picture, along with your complete name, telephone number, email address and date of birth in the body of the email, and &ldquo;Respect the Bun AMOE&rdquo; in the Subject line of the email, and email it to: <a href="mailto:RespectTheBun@brandmovers.com">RespectTheBun@brandmovers.com</a>, to receive one (1) entry into the Contest Portion of the Promotion, and one (1) simultaneous entry into the Sweepstakes Portion of the Promotion for that week&rsquo;s Weekly Sweepstakes Period.<strong> &nbsp;</strong><u>Photo must</u> be received by 11:59:59 AM ET on End Date listed in the Chart to be eligible for a particular Weekly Sweepstakes Period, or by 11:59:59 AM ET on September 5, 2017 for the final Weekly Sweepstakes Period or to be entered into the Contest Portion of the Promotion.</li>
        <li>Sweepstakes Portion Alternate Method of Entry (AMOE) without Photo: To enter only the Sweepstakes Portion of the Promotion without a Photo, send an email with your complete name, telephone number, email address and date of birth in the body of the email, and &ldquo;respect the Bun AMOE&rdquo; in the subject line of the email to <a href="mailto:RespectTheBunSweeps@brandmovers.com">RespectTheBunSweeps@brandmovers.com</a>, to be received by 11:59:59 AM ET on End Date listed in the Chart to be eligible for a particular Weekly Sweepstakes Period, or by 11:59:59 AM ET on September 5, 2017 for the final Weekly Sweepstakes Period.</li>
      </ol>
    </li>
  </ol>
<p><strong>PLEASE NOTE:</strong> <u>Limit one (1) entry into each of the Sweepstakes and Contest Portions of this Promotion (regardless of entry method) per day per person/email address for a total of 113 Weekly Sweepstakes Period entries (7 entries per week with the exception of the final week in which 8 entries is the limit) and 113 Contest entries allowed per person/email address for the entire Promotion Period</u>. &nbsp;Proof of submission of the Photo will not be considered proof of receipt. Any Social Media Photo entry that is submitted without a qualifying unique Photo, does not include the hashtag <strong>#RespectTheBunPromotion, </strong>or does not include a hot dog and/or burger and a Pepperidge Farm&reg; bun will be void. &nbsp;To enter, entrants must adjust their Instagram and Twitter account settings to allow for receipt of messages from Sponsor and/or Administrator. &nbsp;For this purpose, entrants will have to set their Instagram or Twitter account settings to &lsquo;Public', and take any further necessary steps to ensure receipt of any communication from Sponsor and/or Administrator. &nbsp;It is solely entrant's burden to take any steps necessary to adjust their Instagram or Twitter account settings so that messages can be sent by Sponsor and/or Administrator. &nbsp;An entrant's non-receipt of a message from Sponsor and/or Administrator for any reason, including for failure on behalf of entrant to adjust Instagram or Twitter account settings, shall be entrant's, and not Sponsor&rsquo;s or Administrator&rsquo;s fault. &nbsp;Similarly, Sponsor and Administrator shall not be held liable if they are not able to view an entrant's entry for any reason, including the failure on behalf of entrant to adjust their Instagram or Twitter account settings accordingly. By entering this Promotion, each entrant agrees that the Sponsor and/or Administrator has the right to contact the entrant by direct message via entrant&rsquo;s Instagram/Twitter, comment to original entry post on Instagram/Twitter with @tag, or email accounts provided upon registration to administer and fulfill this Promotion.</p>
  <p><strong>This Promotion is not sponsored,  endorsed, administered by or associated with Instagram or Twitter. By entering,  you are providing the information to Sponsor, and not to Instagram or Twitter.  WHEN YOU UPLOAD YOUR PHOTO YOU AUTHORIZE SPONSOR TO USE YOUR PHOTO WITHOUT  COMPENSATION OR ATTRIBUTION.</strong></p>
  <p><strong>Photo Entry Requirements:  </strong>Your  submission must comply with, at the Sponsor&rsquo;s sole discretion, the following  &ldquo;Content Requirements&rdquo; to be eligible for the Contest and/or Sweepstakes, as  applicable.    </p>
  <ul>
    <li>Must be the original creation of the  Entrant;</li>
    <li>Must be in English;</li>
    <li>Must be no more than 3 MB for a photo;</li>
    <li>Must not include any dangerous  activity; </li>
    <li>Must not have been previously published  in any medium (excluding any social media site) or submitted in any other  competition; </li>
    <li>Must not contain material that violates  or infringes another's rights, including but not limited to privacy, publicity  or intellectual property rights, including copyright infringement when used as  provided for in these Official Rules; </li>
    <li>Must not disparage Promotion Entities  or any other person or party affiliated with the Promotion and administration  of this Promotion;</li>
    <li>Must not in any way contain any brand  names, logos or trademarks other than those of Sponsor; </li>
    <li>Must not include personally  identifiable information; </li>
    <li>Must not include or reference anyone  other than entrant unless entrant has written permission (and can provide  evidence of such permission at Sponsor&rsquo;s request and satisfaction) from such  person.  Minors must not appear or be  referenced without that minor&rsquo;s parent&rsquo;s/legal guardian&rsquo;s express written  approval.</li>
    <li>Must not contain material that is unsafe,  inappropriate, indecent, lewd, pornographic, obscene, hateful, tortious,  defamatory, slanderous or libelous; </li>
    <li>Must not contain material that promotes  bigotry, racism, hatred or harm against any group or individual or promotes  discrimination based on race, gender, religion, nationality, disability, sexual  orientation or age; and </li>
    <li>Must not contain material that violates  or is contrary to applicable federal or state law or regulation.</li>
  </ul>
  <p>By entering, you represent and warrant that your entry, including any photo and/or caption, is in compliance with all of the foregoing Photo Entry Requirements. You agree to indemnify, defend and hold Sponsor harmless against any and all liabilities, losses, damages, claims, debts, investigations, fines, penalties, costs, expenses and settlements (including attorneys' fees and costs of litigation, settlement, judgment, interest and penalties) arising out of or related to a breach of the foregoing representations and warranties. By entering, you understand and agree that your entry may be posted on the Website and in any other marketing or promotional materials in any medium now known or hereinafter developed at Sponsor&rsquo;s sole discretion without prior approval or compensation. By entering, you grant to the Sponsor and its licensees a world-wide, perpetual, non-revocable, royalty-free, sub-licensable license to publish your entry as well as derivative works based thereon on the Website, in Sponsor&rsquo;s apps, on social media, and through any and all other media, now known or hereafter devised, in any manner related to the Promotion, without prior notice, approval or compensation.&nbsp;&nbsp; You represent and warrant that you have received all permissions from individuals appearing or referenced in your entry to grant the rights granted herein (and evidence of such rights must be provided upon Sponsor&rsquo;s request) and that Sponsor and its licensee&rsquo;s use of the entry as provided for herein will not violate the rights of any third party. Without limitation, Sponsor reserves the right in its sole discretion to disqualify any entry that, in their sole opinion, refers, depicts or in any way reflects negatively upon the Sponsor, the Promotion or any other person or entity, does not comply with these Official Rules or if Sponsor receives notification about any potential infringements or breaches of law or any other reason set forth herein. Photo&nbsp;entries must be suitable for presentation in a public forum, in sole determination of Sponsor.</p>
  <ol start="4">
    <li><strong>Winner Selections: </strong>
      <ol type="i">
        <li><strong>Contest Judging  Criteria: </strong>There will be a total of one (1) Grand Prize Winner in the  Contest Portion of this Promotion.   During the Contest Period, all eligible Photos/entries will be judged by  members of Sponsor&rsquo;s creative marketing team using the following judging  criteria and percentages: creativity/originality (i.e. a truly unique photo) -  24%; visual/appetite appeal (i.e., a tempting hot dog and/or burger) - 25%; and adherence to  contest theme - 51% (&ldquo;Judging Criteria&rdquo;). No information regarding contest submissions  or judging, other than as otherwise set forth in these Official Rules will be  disclosed and Judges&rsquo; decisions are final.   The top highest scoring Photo entry using the Judging Criteria from all  eligible entries received will be deemed the Grand Prize. Potential Grand Prize  Winners will be contacted via Instagram/Twitter or email (as detailed in  Section 3 above) and may be awarded their prize listed below (subject to  verification of eligibility and compliance with the terms of these Official Rules).  Upon verification, winner will also be posted on Sponsor&rsquo;s Instagram and/or  Twitter Page.  In the event of a tie, the  entrant with the highest score in the visual/appetite appeal criteria will be  deemed the winner from among all tying entrants. In the event a tie still  exists, the entrant with the highest score in the creativity/originality  criteria will be deemed the winning entrant from among such tying entrants.<br>
          <br>
          Failure to respond to the prize notification message within <strong>three (3) days</strong> of notification, or  return of message notification as undeliverable <strong>after three (3) attempts</strong> will result in disqualification and an  alternate potential winner may be awarded, time permitting.</li>
        <li><strong>Sweepstakes  Random Drawings:  </strong>There will be a  total of sixteen (16) First Prize winners in the Sweepstakes portion of this  Promotion (one (1) per Weekly Sweepstakes Period, per the Weekly Sweepstakes  Period chart in Official Rule #2 above (&ldquo;Chart&rdquo;)).  Each First Prize winner will be selected by  random drawing, to be conducted by Administrator, on or about the Random  Drawing date set forth in the Chart, from among all eligible Photos/entries  received up until the End Date of each Weekly Sweepstakes Period. Any  non-winning entries from any Weekly Sweepstakes Period will <u>NOT</u> be  carried over to subsequent Sweepstakes Weekly Entry Periods.  In the event there is not a sufficient amount  of entries received in any particular Sweepstakes Weekly Entry Period, Sponsor  reserves the right not to award a First Prize for that particular Sweepstakes  Weekly Entry Period at Sponsor&rsquo;s sole discretion. </li>
      </ol>
    </li>
  </ol>
  <p>ALL  POTENTIAL WINNERS ARE SUBJECT TO VERIFICATION BY ADMINISTRATOR AND SPONSOR&rsquo;S  DECISIONS ARE FINAL AND BINDING IN ALL MATTERS RELATED TO THE PROMOTION.</p>
  <ol start="5">
    <li><strong>Verification of Potential Winners</strong>: In order to be confirmed as a Winner, and without limitation
      of any other terms herein, the potential Winner must respond to the Instagram/Twitter/email message within
      <strong>3 days</strong> of its receipt by sending an email that contains the Winner&rsquo;s complete name, date
      of birth, mailing address (including street address, city, state, and zip code), and telephone number and send
      it to the email address provided in the Instagram/Twitter/email message. Potential winner may be required to
      provide satisfactory proof, as determined by Sponsor in its sole discretion, that such potential Winner is an
      eligible Entrant, including but not limited to providing a copy of the potential winner&rsquo;s
      government-issued identification and/or additional information required by Sponsor in order to verify
      eligibility and compliance with these Official Rules. Each Winner will be required to sign and return to
      Administrator, within <strong>seven (7) days</strong> of a follow up notice that Administrator will send to
      Winner upon receipt of Winner&rsquo;s verification email, an affidavit of eligibility, and a liability/publicity
      release (except where prohibited) in order to claim his/her prize. If any potential Winner cannot be contacted,
      fails to send an email response to the Instagram/Twitter/email message, or fails to sign and return the
      affidavit of eligibility and liability/publicity release within the required time periods set forth hereunder
      (if required), that potential Winner will forfeit the prize, which may be awarded to an alternate winner, time
      permitting.
    </li>
    <li><strong>PRIZES AND APPROXIMATE RETAIL VALUES (&ldquo;ARV&rdquo;)</strong>:<strong> One (1) Contest Grand
        Prize</strong><strong>: </strong>One (1) Contest Winner will receive a check for $10,000.00. ARV:
      $10,000.00.&nbsp; <strong>Sixteen (16) Sweepstakes Weekly Prizes (one (1) per Sweepstakes Weekly
        Period):&nbsp; </strong>Sixteen (16) Weekly Sweepstakes Winners (one per each week of the Promotion Period)
      will receive a check for $1,000.00. ARV: $1,000.00 each. Limit of one (1) Sweepstakes Weekly Prize per
      person/Twitter/Instagram/email address for the entire Promotion Period. Limit of one (1) Contest Grand Prize per
      person/Twitter/Instagram/email address for the entire Promotion Period. For clarity, a Sweepstakes Weekly Prize
      winner <u>CAN</u> also win the Contest Grand Prize. &nbsp;Total ARV of all prizes is $26,000.00.
    </li>
    <li><strong> CONDITIONS APPLICABLE TO ALL PRIZES: </strong>Prizes are nontransferable, except at Sponsor&rsquo;s
      sole discretion. Prize details and availability are subject to change, in which case Sponsor may substitute a
      prize of equal or greater value. No substitutions or cash redemptions, except at Sponsor&rsquo;s sole
      discretion. Any portion of prize not used by winner is forfeited and no cash substitute will be offered or
      permitted. Prize is provided &ldquo;as is&rdquo; with no warranty or guarantee either express or implied by
      Sponsor. Awarding of all prizes is subject to verification of eligibility, submission of all required Assignment
      and Release Forms, and compliance with these Official Rules and applicable law. Sponsor reserves the right to
      request, in its sole discretion, and Entrant agrees to cooperate with Sponsor&rsquo;s request to have a
      background check done on any Prize winner. All federal, state and local taxes are the sole responsibility of
      winners.
    </li>
    <li><strong>Entry Conditions and Release: </strong>By entering, each entrant agrees to: (a) comply with and be bound
      by these Official Rules and the decisions of the Sponsor which are binding and final in all matters relating to
      this Promotion; (b) release and hold harmless the Promotion Entities from and against any and all claims,
      expenses, and liability, including but not limited to negligence and damages of any kind to persons and
      property, including but not limited to invasion of privacy (under appropriation, intrusion, public disclosure of
      private facts, false light in the public eye or other legal theory), defamation, slander, libel, violation of
      right of publicity, infringement of trademark, copyright or other intellectual property rights, property damage,
      or death or personal injury arising out of or relating to a participant's entry, creation of an entry or Photo
      of an entry, participation in the Promotion, the Photo, acceptance or use or misuse of prize &nbsp;and/or the
      broadcast, exploitation or use of entry or the Photo; and (c) indemnify, defend and hold harmless the Promotion
      Entities from and against any and all claims, expenses, and liabilities (including reasonable attorneys fees)
      arising out of or relating to an entrant's participation in the Promotion and/or entrant's acceptance, use or
      misuse of prize or any portion thereof.&nbsp;</li>
    <li><strong>Publicity:</strong>Acceptance of any Prize shall constitute and signify each Winner&rsquo;s agreement
      and consent that Sponsor and its designees may use the winner&rsquo;s name, city, state, likeness, photo
      (including Social Account profile photo), and/or prize information in connection with the Promotion for
      promotional, advertising or other purposes, worldwide, in any and all media now known or hereafter devised,
      including the Internet, without limitation and without further payment, notification, permission or other
      consideration, except where prohibited by law.
    </li>
    <li><strong>General Conditions: </strong>Sponsor reserves the right to cancel, suspend and/or modify the Promotion,
      or any part of it, for any reason whatsoever, including, without limitation, fire, flood, natural or man-made
      epidemic of health of other means, earthquake, explosion, labor dispute or strike, act of God or public enemy,
      satellite or equipment failure, riot or civil disturbance, terrorist threat or activity, war (declared or
      undeclared) or any federal state or local government law, order, or regulation, public health crisis, order of
      any court or jurisdiction or if any fraud, technical failures or any other factor beyond Sponsor's reasonable
      control impairs the integrity or proper functioning of the Promotion, as determined by Sponsor in its sole
      discretion. If the Promotion is terminated before the designated end date, Sponsor will (if possible) select the
      winner using the Judging Criteria above from all eligible, non-suspect Promotion entries received as of the date
      of the event giving rise to the termination, and by random drawing for any remaining Sweepstakes eligible for
      the Contest Portion of the Promotion, non-suspect Sweepstakes entries received as of the date of the event
      giving rise to the termination for the Sweepstakes portion of the Promotion.&nbsp; Inclusion in such judging
      shall be each entrant&rsquo;s sole and exclusive remedy under such circumstances.&nbsp; Sponsor reserves the
      right in its sole discretion to disqualify any individual it finds to be tampering with the entry process or the
      operation of the Promotion or to be acting in violation of these Official Rules or any other promotion or in an
      unsportsmanlike or disruptive manner. Any attempt by any person to deliberately undermine the legitimate
      operation of the Promotion may be a violation of criminal and civil law, and, should such an attempt be made,
      Sponsor reserves the right to seek damages from any such person to the fullest extent permitted by law. Only the
      type and quantity of prizes described in these Official Rules will be awarded.&nbsp; The invalidity or
      unenforceability of any provision of these rules shall not affect the validity or enforceability of any other
      provision.&nbsp; In the event that any provision is determined to be invalid or otherwise unenforceable or
      illegal, these rules shall otherwise remain in effect and shall be construed in accordance with their terms as
      if the invalid or illegal provision were not contained herein.&nbsp; Sponsor's failure to enforce any term of
      these Official Rules shall not constitute a waiver of that provision.&nbsp;</li>
    <li><strong>Limitations of Liability: </strong>The Promotion Entities as set forth above are not responsible for:
      (1) any incorrect or inaccurate information, whether caused by entrants, printing errors or by any of the
      equipment or programming associated with or utilized in the Promotion; (2) technical failures of any kind,
      including, but not limited to malfunctions, interruptions, or disconnections in phone lines or network hardware
      or software; (3) unauthorized human intervention in any part of the entry process or the Promotion; (4)
      technical or human error which may occur in the administration of the Promotion or the processing of entries; or
      (5) any injury or damage to persons or property which may be caused, directly or indirectly, in whole or in
      part, from entrant's participation in the Promotion or receipt or use or misuse of any Prize. If for any reason
      an entry is confirmed to have been erroneously deleted, lost, or otherwise destroyed or corrupted, entrant's
      sole remedy is another entry in the Promotion, provided that if it is not possible to award another entry due to
      discontinuance of the Promotion, or any part of it, for any reason, Sponsor, at its discretion, may elect to
      hold judging from among all eligible entries received up to the date of discontinuance for any or all of the
      Prizes offered herein. Entrant further agrees and acknowledges that Sponsor reserves the right to forfeit or
      award any unclaimed or leftover Prize at its sole discretion. &nbsp;</li>
    <li><strong>Disputes/Governing Law: </strong>Entrant agrees that: (i) any and all disputes, claims and causes of
      action arising out of or connected with this Promotion, other than those concerning the administration of the
      Promotion or the determination of winner or for any disputes arising from the loss or injury from the
      participation in a Prize, shall be resolved individually, without resort to any form of class action; (ii) any
      disputes arising out of these Official Rules (except for any disputes arising from the loss or injury from the
      use of Prizes) shall be submitted to final, binding arbitration conducted in Illinois, under the Arbitration
      Rules and Procedures of the Judicial Arbitration and Mediation Services Inc. before a single, neutral arbitrator
      who is a former or retired Illinois state or federal court judge with experience in entertainment matters who
      shall in New Jersey law and the Federal Rules of Evidence and have no authority to award punitive damages.
      Either party may enforce a final arbitration award in any court of competent jurisdiction in New Jersey, City of
      Camden, including an award of costs, fees and expenses incurred in enforcing the award. Notwithstanding the
      foregoing, Promotion Entities shall be entitled to seek injunctive relief (unless otherwise precluded by any
      other provision of these Official Rules) in the state and federal courts of Illinois. Any dispute or portion
      thereof, or any claim for a particular form of relief (not otherwise precluded by any other provision of these
      Official Rules), that may not be arbitrated pursuant to applicable state or federal law may be heard only in a
      court of competent jurisdiction in Camden, New Jersey; (iii) any and all claims, judgments and awards shall be
      limited to actual out-of- pocket costs incurred, including costs associated with entering this Promotion, but in
      no event attorneys' fees; and (iv) under no circumstances will entrant be permitted to obtain awards for, and
      entrant hereby waives all rights to claim punitive, incidental and consequential damages and any other damages,
      other than for actual out- of-pocket expenses, and any and all rights to have damages multiplied or otherwise
      increased. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATIONS OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR
      CONSEQUENTIAL DAMAGES, SO THE ABOVE MAY NOT APPLY TO YOU. All issues and questions concerning the construction,
      validity, interpretation and enforceability of these Official Rules, or the rights and obligations of the
      entrant and Sponsor in connection with the Promotion, shall be governed by, and construed in accordance with,
      the laws of the State of in New Jersey, without giving effect to any choice of law or conflict of law rules
      (whether of the State of New Jersey or any other jurisdiction), which would cause the application of the laws of
      any jurisdiction other than the State of New Jersey.
    </li>
    <li><strong>Privacy Policies and Data Collection: </strong>Information provided by you for this Promotion is subject
      to Sponsor&rsquo;s privacy policy located at: <a href="http://www.campbellsoupcompany.com/privacy_policy.asp">http://www.campbellsoupcompany.com/privacy_policy.asp</a>.
      By entering this Promotion, each entrant agrees that the Sponsor, or Administrator on behalf of Sponsor, has the
      right to contact the entrant by phone, direct message, or email accounts provided during entry to administer and
      fulfill this Promotion.
    </li>
    <li><strong>Promotion Winners: </strong>For the Winners&rsquo; names (available after September 30, 2017), send a
      hand-printed, self-addressed, stamped envelope by September 12, 2017 to: Winners List, Pepperidge Farm Respect
      the Bun Promotion, c/o Brandmovers, Inc., 590 Means Street, Suite 250, Atlanta, GA 30318.<strong>
        &nbsp; </strong></li>
    <li><strong>Official Rules: </strong>For a copy of these Official Rules send a hand-printed, self-addressed, stamped
      envelope by September 12, 2017 to: Official Rules &ndash; Pepperidge Farm Respect the Bun Promotion, c/o
      Brandmovers, Inc., 590 Means Street, Suite 250, Atlanta, GA 30318.<strong>&nbsp; </strong>A copy of the Rules
      may also be obtained by printing them from this Website.&nbsp;</li>
    <li><strong>Sponsor:</strong> Pepperidge Farm, Incorporated, 595 Westport Ave, Norwalk CT 06851</li>
    <li><strong>Administrator:</strong> Brandmovers, Inc., 590 Means Street, Suite 250, Atlanta, GA 30318.</li>
  </ol>
</div>
