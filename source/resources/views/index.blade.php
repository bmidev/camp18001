@extends('layout.master')

@section('content')

<!-- <section class="intro" id="intro" data-stellar-background-ratio="0.9"> -->
<section class="intro" id="intro">
    <div class="container tac">
        <div class="row">
            <div class="col-sm-6 content right">
                @if($promo_over) {{$promo_over}}
                    <div class="banner-container">
                        {{--<p>This summer, we're in search of the best looking burger or hot dog in the country. <span>Show us what you got for a chance to win big!</span></p>--}}
                        {{--<div class="hr visible-xs">--}}
                            {{--<img src="{{ url('./img/hr-underline.png') }}" alt="Underline">--}}
                        {{--</div>--}}
                        {{--<h1>Weekly winners</h1>--}}
                        {{--<h2 class="hidden-mobile">Get $1,000!</h2>--}}
                        {{--<div class="prize-tac hidden-sm">Get $1,000!</div>--}}
                        <h3>The Respect The Bun Promotion has ended. Thank you for your participation!</h3>
                        <div class="hr">
                            <img src="{{ url('./img/hr-underline.png') }}" alt="Underline">
                        </div>
                        {{--<h4>From May 14 until Sep. 4, <span>take a pic of a burger or hot dog</span> proudly served on a Pepperidge Farm<sup>®</sup>--}}
                            {{--bun, then <span>post it to Instagram or Twitter.</span></h4>--}}
                        {{--<p class="lead">{!! trans('index.landing_lead') !!}</p>--}}
                        <h5>
                            <a target="_blank" href="https://www.instagram.com/pepperidgefarm/"><span class="icon-instagram"></span></a>
                            <a target="_blank" href="https://twitter.com/pepperidgefarm/"><span class="icon-twitter"></span></a>
                            <a target="_blank" href="https://www.instagram.com/explore/tags/RespectTheBunPromotion/"><span class="hashtag">#RespectTheBunPromotion</span></a>
                        </h5>
                    </div>
                @else
                    <div class="banner-container">
                        <p>This summer, we're in search of the best looking burger or hot dog in the country. <span>Show us what you got for a chance to win big!</span></p>
                        <div class="hr visible-xs">
                            <img src="{{ url('./img/hr-underline.png') }}" alt="Underline">
                        </div>
                        <h1>Weekly winners</h1>
                        <h2 class="hidden-mobile">Get $1,000!</h2>
                        <div class="prize-tac hidden-sm">Get $1,000!</div>
                        <h3>The Grand Prize winner gets $10,000!</h3>
                        <div class="hr">
                            <img src="{{ url('./img/hr-underline.png') }}" alt="Underline">
                        </div>
                        <h4>From May 14 until Sep. 4, <span>take a pic of a burger or hot dog</span> proudly <br>served  on a Pepperidge Farm<sup>®</sup>
                            bun, then <span>post it to Instagram or Twitter.</span></h4>
                        {{--<p class="lead">{!! trans('index.landing_lead') !!}</p>--}}
                        <h5>
                            <a target="_blank" href="https://www.instagram.com/pepperidgefarm/"><span class="icon-instagram"></span></a>
                            <a target="_blank" href="https://twitter.com/pepperidgefarm/"><span class="icon-twitter"></span></a>
                            <a target="_blank" href="https://www.instagram.com/explore/tags/RespectTheBunPromotion/"><span class="hashtag">#RespectTheBunPromotion</span></a>
                        </h5>
                    </div>
                @endif
                {{--<p><a href="#how-to-enter" class="btn">{{ trans('index.nav_how_to_enter') }}</a></p>--}}
            </div>
            {{--<div class="col-sm-12 banner">--}}
            {{--<div class="content-1"><img src="{{ url('./img/' . $dateString . '/entry1.jpg') }}" alt=""></div>--}}
            {{--<div class="content-2 show-for-small"><img src="{{ url('./img/' . $dateString . '/entry3') }}{{ ($dateString == "2016_10") ? ".gif" : ".jpg" }}" alt=""></div>--}}
            {{--<div class="content-2 hide-for-small"><img src="{{ url('./img/' . $dateString . '/entry2') }}{{ ($dateString == "2016_10") ? ".gif" : ".jpg" }}" alt=""></div>--}}
            {{--<div class="double-enclosure">--}}
            {{--<div class="enclosure">--}}
            {{--<div class="text-content">--}}
            {{--<div class="month">{{ trans('index.month_' . $month) }}</div>--}}
            {{--<h4>{{ trans('index.landing_monthly_challenge') }}</h4>--}}
            {{--<h5 class="text-blue">{!! trans('index.landing_challenge_name_' . $dateString) !!}</h5>--}}
            {{--<p class="challenge-copy">{!! trans('index.landing_create_a_tale_' . $dateString) !!}</p>--}}
            {{--<p><a href="#gallery" class="btn btn-caret btn-light">{{ trans('index.landing_see_more_examples') }}</a></p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="content-3 hide-for-small"><img src="{{ url('./img/' . $dateString . '/entry3') }}{{ ($dateString == "2016_10") ? ".gif" : ".jpg" }}" alt=""></div>--}}
            {{--<div class="content-3 show-for-small"><img src="{{ url('./img/' . $dateString . '/entry2') }}{{ ($dateString == "2016_10") ? ".gif" : ".jpg" }}" alt=""></div>--}}
            {{--<div class="content-4"><img src="{{ url('./img/' . $dateString . '/entry4.jpg') }}" alt=""></div>--}}
            {{--<div class="clearfix"></div>--}}
            {{--<div class="intro-bottom-image"></div>--}}
            {{--<div class="download-image">--}}
            {{--*<a href="{{ url('downloads/Goldfish_Cracker.pdf') }}" target="_blank">{!! trans('index.download_image_click_here') !!}</a> {!! trans('index.download_image_desc') !!}--}}
            {{--</div>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="down-btn">
        <p>Inspiration</p>

        <div class="down-container">
            <a href="#gallery"><img src="{{ url('./img/icon-down.png') }}" alt="down-arrow"></a>
        </div>

    </div>
    {{--<div class="rules"><p>*<a href="{{ route('rules') }}" class="rules-popup" ng-click="openRules($event)">Abbreviated Rules</a></p></div>--}}
    <div class="rules">
        <p class="note">NO PURCHASE NECESSARY. Open only to legal residents of the 50 US and DC, who are 18 + (or 19 +
            if resident of AL &amp; NE). Void where prohibited. Begins 5/14/18 at 12:00:00 PM ET and ends 9/4/18 at 11:59:59
            AM ET. For full rules and Alternate Method of Entry, visit <a href="{{ LaravelLocalization::getCurrentLocale() }}/rules" ng-click="openRules($event)">Official Rules</a>. Sponsor:
            Pepperidge Farm, Incorporated.</p>
    </div>
</section>

<section class="gallery" id="gallery" ng-controller="GalleryController">
    <div class="container">
        <div class="tac">Gallery</div>
        <p>The bun matters and we’ve got the pictures to prove it. Check out these juicy examples for inspiration, then
            get to work on your burger or hot dog.</p>
    </div>

    <div class="gallery-mosaic tal">
        <div>
            <div class="mix-container">
                <div class="controls">
                    <label class="rel">Filter: </label>
                    <select ng-model="gallery.filter" ng-change="updateFilter()">
                        <option value="">Show All</option>
                        @foreach($weeks as $week)
                        <option value="{{$week->id}}">Week {{$week->id}}</option>
                        @endforeach
                    </select>
                    {{--<div class="button-holder">--}}
                    {{--<button class="filter" ng-class="{active:gallery.filter==null}" ng-click="setFilter()">{{ trans('index.gallery_filter_all') }}</button>--}}
                    {{--<button class="filter" ng-class="{active:gallery.filter=='video'}" ng-click="setFilter('video')">{{ trans('index.gallery_filter_videos') }}</button>--}}
                    {{--<button class="filter" ng-class="{active:gallery.filter=='photo'}" ng-click="setFilter('photo')">{{ trans('index.gallery_filter_photos') }}</button>--}}
                    {{--<button class="filter" ng-class="{active:gallery.filter=='winners'}" ng-click="setFilter('winners')">{{ trans('index.gallery_filter_winners') }}</button>--}}
                    {{--</div>--}}
                </div>
                <div class="mix-inner-container container-fluid"
                     ng-class="{opened: gallery.expanded, 'has-more':gallery.hasMoreItems}"
                     ng-show="gallery.items && gallery.items.length > 0">
                    <div social-gallery mixitup='mixitup' class="container-fluid gallery-items"
                         entities='gallery.items'>
                        <div class="grid-sizer"></div>
                        <div class="mix pointer"
                             ng-repeat="item in gallery.items track by $index"
                             ng-class="{
                                    'category-winner contest-winner': item.banner.contest_winner,
                                    'category-Winners sweepstakes-winner': item.banner.sweepstakes_winner,
                                    'pepperidge-inspired': item.banner.pepperidge_inspired,
                                    'large': item.featured,
                                    'video': item.video
                                    }"
                             ng-click="openModal(item, $index)">

                            {{--<div class="thumb"><img ng-src="<% item.image %>" alt=""></div>--}}
                            <div class="thumb">
                                <div class="img-container"
                                     ng-style="{'background-image': 'url(' + item.image + ')'}"></div>
                            </div>
                            <div ng-show="!item.featured">
                                {{--<hr>--}}
                                <div class="content">
                                    <div class="time" ng-bind="item.date | timeAgo"></div>
                                    {{--<div class="description" ng-bind-html="item.comments | nl2br"></div>--}}
                                    <div class="submitter" ng-bind="'@' + item.author.username"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bottom-underline">
                <div ng-show="gallery.hasMoreItems">
                    <p class="tac ttu"><span class="btn btn-lg" ng-click="loadMore()">Load More</span></p>
                </div>

                <div ng-show="gallery.items == null || gallery.items.length == 0" class="no-matches">
                    - No Matches -
                </div>
            </div>
            <div class="legend-container">
                <div class="legend-img">
                    <img src="{{ url('./img/icon-team-post.png') }}" alt="Posted by our team logo">
                </div>
                <p>From Our Team</p>
            </div>


        </div>

        {{--<div class="text-center" ng-show="mobile">--}}
        {{--<div class="mobile-filter">--}}
        {{--<div class="select-label">--}}
        {{--<span ng-show="gallery.filter==null">{{ trans('index.gallery_filter_by') }}</span>--}}
        {{--<span ng-show="gallery.filter=='video'">{{ trans('index.gallery_filter_videos') }}</span>--}}
        {{--<span ng-show="gallery.filter=='photo'">{{ trans('index.gallery_filter_photos') }}</span>--}}
        {{--<span ng-show="gallery.filter=='winners'">{{ trans('index.gallery_filter_winners') }}</span>--}}
        {{--<span class="caret-drop">--}}
        {{--<span class="caret"></span>--}}
        {{--</span>--}}
        {{--</div>--}}
        {{--<select ng-model="gallery.filter" ng-change="updateFilter()">--}}
        {{--<option value="">{{ trans('index.gallery_filter_all') }}</option>--}}
        {{--<option value="video">{{ trans('index.gallery_filter_videos') }}</option>--}}
        {{--<option value="photo">{{ trans('index.gallery_filter_photos') }}</option>--}}
        {{--<option value="winners">{{ trans('index.gallery_filter_winners') }}</option>--}}
        {{--</select>--}}
        {{--</div>--}}

        {{--<div ng-show="gallery.items == null || gallery.items.length == 0" class="no-matches">--}}
        {{--- No Matches ---}}
        {{--</div>--}}
        {{--</div>--}}

        {{--<div ng-if="gallery.showSlides && mobile">--}}

        {{--<slick slides-to-show="1" init-onload="true" data="gallery.items">--}}
        {{--<div ng-repeat="slide in slides track by $index"--}}
        {{--ng-class="">--}}
        {{--<div class="flex-container">--}}
        {{--<div class="mix" ng-repeat="item in slide track by $index" ng-init="$innerIndex = $index"--}}
        {{--ng-class="{--}}
        {{--'category-winner contest-winner': item.banner.contest_winner,--}}
        {{--'category-Winners sweepstakes-winner': item.banner.sweepstakes_winner,--}}
        {{--'goldfish-inspired': item.banner.goldfish_inspired,--}}
        {{--'large': item.featured}"--}}
        {{--ng-click="openModal(item, getIndex($index, $parent.$index))">--}}
        {{--<div class="thumb"><img ng-src="<% item.thumbnail %>" alt=""></div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</slick>--}}
        {{--</div>--}}

        {{--<div class="tag" ng-hide="desktop">{!! trans('index.gallery_inspiration_from_goldfish') !!}</div>--}}
    </div>
    {{--<div class="container tac">--}}
    {{--<div class="small-print">--}}
    {{--<p><small><a href="http://www.campbellsoupcompany.com/terms-of-use" target="_blank">{{ trans('index.gallery_terms_of_use') }}</a> | <a href="http://www.campbellsoupcompany.com/privacy-policy" target="_blank">{{ trans('index.gallery_privacy_policy') }}</a><br>--}}
    {{--{!! trans('index.gallery_terms_' . $dateString) !!}</small></p>--}}
    {{--</div>--}}
    {{--</div>--}}
    <div class="rules">
    	<p class="note">NO PURCHASE NECESSARY. Open only to legal residents of the 50 US and DC, who are 18 + (or 19 +
            if resident of AL &amp; NE). Void where prohibited. Begins 5/14/18 at 12:00:00 PM ET and ends 9/4/18 at 11:59:59
            AM ET. For full rules and Alternate Method of Entry, visit <a href="{{ LaravelLocalization::getCurrentLocale() }}/rules" ng-click="openRules($event)">Official Rules</a>. Sponsor:
            Pepperidge Farm, Incorporated.</p>
    </div>
</section>

<section class="how-to-enter" id="how-to-enter">
    <div class="outer-container">
        <div class="container">
            {{--<div class="fish fish1" data-stellar-ratio="1.4"></div>--}}
            {{--<div class="fish fish2" data-stellar-ratio=".7"></div>--}}
            <div class="tac">How to enter</div>
            <div class="hr">
                <img src="{{ url('./img/hr-brown-underline.png') }}" alt="Underline">
            </div>
            <table>
                <tbody>
                    <tr>
                        <td><span class="numb">1</span></td>
                        <td class="text">Create a mouthwatering burger or hot dog served on a Pepperidge Farm<sup>®</sup> bun.*</td>
                    </tr>
                    <tr>
                        <td><span class="numb">2</span></td>
                        <td class="text">Pose your burger or hot dog lovingly. Take a picture.</td>
                    </tr>
                    <tr>
                        <td><span class="numb">3</span></td>
                        <td class="text">Post your pic to Instagram or Twitter with a <span>#RespectTheBunPromotion</span> hashtag.</td>
                    </tr>
                    <tr>
                        <td><span class="numb">4</span></td>
                        <td class="text">Repeat. (You can post one unique photo per day.)</td>
                    </tr>
                </tbody>
            </table>
            {{--<ol>--}}
                {{--<li>Create a mouthwatering burger or hot dog served on a Pepperidge Farm<sup>®</sup> bun.*</li>--}}
                {{--<li>Pose your burger or hot dog lovingly. Take a picture.</li>--}}
                {{--<li>Post your pic to Instagram or Twitter with a <span>#RespectTheBunPromotion</span> hashtag.</li>--}}
                {{--<li>Repeat. (You can post one unique photo per day.)</li>--}}
            {{--</ol>--}}
            {{--<ol>--}}
            {{--<li><p>{!! trans('index.how_to_step1') !!}</p></li>--}}
            {{--<li class="clearfix">--}}
            {{--<div class="pull-left">--}}
            {{--<p>{!! trans('index.how_to_step2') !!} <br>--}}
            {{--<span class="promo text-center">--}}
            {{--<span>#GoldfishTalesPromotion</span>--}}
            {{--<a target="_blank" href="https://twitter.com/search?f=tweets&q=%23GoldfishTalesPromotion"><span class="icon-twitter"></span></a>--}}
            {{--<a target="_blank" href="https://instagram.com/explore/tags/goldfishtalespromotion/"><span class="icon-instagram"></span></a>--}}
            {{--</span>--}}
            {{--</p>--}}
            {{--</div>--}}
            {{--</li>--}}
            {{--<li><p>{!! trans('index.how_to_step3') !!}</p></li>--}}
            {{--</ol>--}}
            {{--<div class="theme tac">--}}
            {{--<div class="content-1"><img src="{{ url('./img/'. $dateString .'/entry1.jpg') }}" alt=""></div>--}}
            {{--<div class="content-2"><img src="{{ url('./img/'. $dateString .'/entry2') }}{{ ($dateString == "2016_10") ? ".gif" : ".jpg" }}" alt=""></div>--}}
            {{--<div class="double-enclosure">--}}
            {{--<div class="enclosure">--}}
            {{--<div class="text-content month-theme">--}}
            {{--<div class="month">{{ trans('index.month_' . $month) }}</div>--}}
            {{--<h5>--}}
            {{--{{ trans('index.how_to_this_months_theme') }}--}}
            {{--<div class="theme-name">{!! trans('index.landing_challenge_name_' . $dateString) !!}</div>--}}
            {{--</h5>--}}
            {{--<p><a href="#gallery" class="btn btn-caret btn-light">{{ trans('index.landing_see_more_examples') }}</a></p>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--</div>--}}
            {{--<div class="content-3"><img src="{{ url('./img/'. $dateString .'/entry3') }}{{ ($dateString == "2016_10") ? ".gif" : ".jpg" }}" alt=""></div>--}}
            {{--<div class="content-4"><img src="{{ url('./img/'. $dateString .'/entry4.jpg') }}" alt=""></div>--}}
            {{--</div>--}}
            {{--<div class="may-appear">--}}
            {{--<p>{!! trans('index.how_to_may_appear') !!}</p>--}}
            {{--<div><small>{!! trans('index.how_to_may_appear_small') !!}</small></div>--}}
            {{--</div>--}}
            {{--<div class="small-print tac">--}}
            {{--<p class="ff-hand"><small><a href="{{ route('rules') }}" ng-click="openRules($event)" class="btn btn-caret btn-smaller rules-popup">{{ trans('index.how_to_see_rules') }}</a></small></p>--}}
            {{--<p><small><a href="http://www.campbellsoupcompany.com/terms-of-use" target="_blank">{{ trans('index.gallery_terms_of_use') }}</a> | <a href="http://www.campbellsoupcompany.com/privacy-policy" target="_blank">{{ trans('index.gallery_privacy_policy') }}</a><br>--}}
            {{--{!! trans('index.gallery_terms_' . $dateString) !!}</small></p>--}}
            {{--</div>--}}
        </div>
    </div>
    <div class="rules"><p>*See <a href="{{ route('rules') }}" class="rules-popup" ng-click="openRules($event)">Official Rules</a> to learn how to enter the Sweepstakes without a photo and the contest without a Pepperidge Farm<sup>&reg;</sup> bun.</p>
    	<p class="note">NO PURCHASE NECESSARY. Open only to legal residents of the 50 US and DC, who are 18 + (or 19 +
            if resident of AL &amp; NE). Void where prohibited. Begins 5/14/18 at 12:00:00 PM ET and ends 9/4/18 at 11:59:59
            AM ET. For full rules and Alternate Method of Entry, visit <a href="{{ LaravelLocalization::getCurrentLocale() }}/rules" ng-click="openRules($event)">Official Rules</a>. Sponsor:
            Pepperidge Farm, Incorporated.</p>
    </div>
</section>

<!-- <section class="prizes" id="prizes" data-stellar-background-ratio="0.8"> -->
<section class="prizes" id="prizes">
    <div class="container tac">
        <div class="header">
            <div class="tac">Prizes</div>
            {{--<p><strong>{{ trans('index.prizes_two_ways_to_win') }}</strong></p>--}}
            {{--<p><a href="{{ route('rules') }}" ng-click="openRules($event)" class="btn btn-caret btn-smaller rules-popup">{{ trans('index.how_to_see_rules') }}</a></p>--}}
        </div>
        <div class="hr visible-xs">
            <img src="{{ url('./img/hr-underline-prize.png') }}" alt="Underline">
        </div>
        <div class="row">
            <div class="col-sm-6 weekly">
                <div class="prize-container">
                    <h2>Weekly Prize</h2>
                    <p>From now until September, one lucky winner will be randomly selected each week (Mon. to Mon.)</p>
                    <h3>$1,000 each</h3>
                </div>

                {{--<div class="prize-content">--}}
                {{--<h5>{{ trans('index.prizes_contest_prize') }}</h5>--}}
                {{--<div class="img-holder"></div>--}}
                {{--<p><small>({{ trans('index.prizes_1_winner') }})</small></p>--}}
                {{--<p>{!! trans('index.prizes_submit') !!}</p>--}}
                {{--<div class="month-theme">--}}
                {{--<div class="month">{{ trans('index.month_' . $month) }}</div>--}}
                {{--<h5 class="text-tertiary">--}}
                {{--{{ trans('index.how_to_this_months_theme') }}--}}
                {{--<div class="theme-name">{!! trans('index.landing_challenge_name_' . $dateString) !!}</div>--}}
                {{--</h5>--}}
                {{--<p><a href="#gallery" class="btn btn-caret">{{ trans('index.prizes_see_examples') }}</a></p>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
            <div class="col-sm-6 grand">
                <div class="prize-container">
                    <h2>Grand Prize</h2>
                    <p>The best of the best burger or hot dog will be selected by us to win the ultimate prize. Could it be
                        yours?</p>
                    <h3>$10,000</h3>
                </div>
                {{--<div class="prize-content">--}}
                {{--<h5>{{ trans('index.prizes_sweepstakes_prize') }}</h5>--}}
                {{--<div class="img-holder"></div>--}}
                {{--<p><small>({{ trans('index.prizes_5_winners') }})</small></p>--}}
                {{--<p>{!! trans('index.prizes_great_way') !!}</p>--}}
                {{--<div class="awarded-as">--}}
                {{--{{ trans('index.prizes_awarded_as') }}--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <div class="rules"><p>NO PURCHASE NECESSARY. Open only to legal residents of the 50 US and DC, who are 18 + (or 19 +
            if resident of AL & NE). Void where prohibited. Begins 5/14/18 at 12:00:00 PM ET and ends 9/4/18 at 11:59:59
            AM ET. For full rules and Alternate Method of Entry, visit <a href="{{ LaravelLocalization::getCurrentLocale() }}/rules" ng-click="openRules($event)">Official Rules</a>. Sponsor:
            Pepperidge Farm, Incorporated.</p></div>
</section>
{{--<section class="rules" id="rules"></section>--}}
@endsection

