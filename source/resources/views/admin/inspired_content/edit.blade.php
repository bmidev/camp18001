@extends('layout.admin')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            Add Inspired Content
        </div>
        <div class="panel-body">
            <h3>Add Inspired Content</h3>

            {!! \Html::ul($errors->all()) !!}

            {!! Form::open(['url' => route('admin.inspired_content.update', $post->id), 'class' => 'form-horizontal', 'files' => true, 'method' => 'put']) !!}

            <div class="form-group">
                {!! Form::label('comments', 'Comments', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::textarea('comments', $post->comments, ['class' => 'form-control', 'required'=>'']) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('file', 'Current Photo', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    <img src="{{ $post->image }}" width="200" />
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('file', 'Update Photo', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-10">
                    {!! Form::file('file', Input::old('photo_url'), array('class' => 'form-control')) !!}
                </div>
            </div>

            <div class="row form-footer">
                <div class="col-10-sm col-sm-offset-2">
                    {!! Form::submit('Update Post', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

@endsection