@extends('layout.admin')


@section('content')

    <div class="social-posts panel panel-default">
        <div class="panel-heading"><span class="glyphicon glyphicon-th-list"></span> Moderate Posts</div>

        <table class="table table-striped">
            <thead>
            <tr>
                <th class="col-xs-2" class="text-center">Image/Video</th>
                <th class="col-xs-5">Content</th>
                <th class="col-xs-2 text-center">Gallery</th>
                <th class="col-xs-2 text-center">Contest</th>
                <th class="col-xs-1 text-center">Status</th>
            </tr>
            </thead>
            <tbody>
            @if( count($posts) == 0 )
                <tr>
                    <td colspan="6" class="no-matches">No Matching Posts</td>
                </tr>
            @else
                @foreach( $posts as $p )
                    <tr class="{{ $p->valid_entry === 0 ? "danger" : ""}}">
                        <td>
                            @if( $p->isImage() )
                                <a class="social-image" href="{{ $p->image }}">
                                    <img src="{{ $p->image }}" alt="" />
                                </a>
                            @elseif( $p->isVideo() )
                                <a class="social-image" href="{{ $p->image }}">
                                    <video width="320" height="320" controls>
                                        <source src="{{ $p->video }}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                </a>
                            @else
                                N/A
                            @endif
                        </td>
                        <td>
                            <img class="author-image" src="{{ $p->author_image }}" alt="" />
                            <a href="{{ $p->author_profile }}" target="_blank">{{ $p->author_username }}</a>
                            ({{ $p->author_name }})
                            <hr/>
                            {{ $p->comments }}
                            <hr/>
                            <a href="{{ $p->url }}" target="_blank">View on {{ $p->getNetwork() }}</a><br/>
                            {{ date("m/d/y g:i A", strtotime($p->post_date)) }}
                        </td>
                        <td class="text-center">
                            <label>
                                <input type="checkbox" value="1" class="show-gallery" /><br/>
                                Gallery
                            </label>
                        </td>
                        <td class="text-center">
                            <label>
                                <input type="checkbox" value="1" class="contest-eligible" /><br/>
                                Contest
                            </label>
                        </td>
                        <td class="text-center">
                            <div>
                                <span class="btn btn-success approve-post btn-block" data-post-id="{{ $p->id }}">Approve</span>
                                <span class="btn btn-danger reject-post btn-block" data-post-id="{{ $p->id }}">Reject</span>
                            </div>

                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>

    </div>

@endsection


@section('scripts')
    <script>
        $(function() {

            $('.approve-post').on('click', function() {
                var $row = $(this),
                        id = $(this).attr('data-post-id'),
                        showGallery = $(this).closest('tr').find('.show-gallery').is(':checked'),
                        contestEligible = $(this).closest('tr').find('.contest-eligible').is(':checked');

                $.post(BASE_URL + '/admin/posts/' + id + '/moderate', {
                    approved: 1,
                    show_gallery: showGallery ? 1 : 0,
                    contest_eligible: contestEligible ? 1 : 0,
                    _token: _token
                }, function(response) {
                    $row.closest('tr').fadeOut(function() {
                        $row.remove();
                    })
                });
            });

            $('.reject-post').on('click', function() {
                var $row = $(this);
                var id = $(this).attr('data-post-id');

                $.post(BASE_URL + '/admin/posts/' + id + '/moderate', {
                    approved: 0,
                    show_gallery: 0,
                    contest_eligible: 0,
                    _token: _token
                }, function(response) {
                    $row.closest('tr').fadeOut(function() {
                        $row.remove();
                    })
                });
            });

            // Lightbox
            $('.social-posts table').magnificPopup({
                delegate: '.social-image',
                type: 'image',
                gallery: {
                    enabled: true
                }
            });

        });
    </script>
@endsection