<!doctype html>
<html class="no-js" lang="{{ LaravelLocalization::getCurrentLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Respect The Bun</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="p:domain_verify" content="87322e860683586e1eba6b0437d568be"/>
    <meta property="fb:app_id" content="996599403810733" />
    <meta property="og:url" content="{{ url() }}"/>
    <meta property="og:image" content="{{ url('img/logo.png') }}"/>
    <meta property="og:title" content="Respect The Bun"/>
    <meta property="og:description" content="Show us your burger for a chance to win. $1,000 each week! $10,000 grand prize! #RespecttheBunPromotion"/>
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script>
        var BASE_URL = "{{ secure_url('/') }}";
        var LOCALE = "{{ LaravelLocalization::getCurrentLocale() }}";
    </script>
</head>
<body ng-app="app" class="{{ LaravelLocalization::getCurrentLocale() }}" ng-controller="PageController">
{{--<script type='text/javascript'>--}}
    {{--var ebRand = Math.random() + '';--}}
    {{--ebRand = ebRand * 1000000;--}}
    {{--document.write('<scr' + 'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=900694&amp;rnd=' + ebRand + '"></scr' + 'ipt>');--}}
{{--</script>--}}
{{--<noscript>--}}
    {{--<img width="1" height="1" style="border:0"--}}
         {{--src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=900694&amp;ns=1"/>--}}
{{--</noscript>--}}
{{--<script type='text/javascript'>--}}
    {{--var ebRand = Math.random() + '';--}}
    {{--ebRand = ebRand * 1000000;--}}
    {{--document.write('<scr' + 'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=900695&amp;rnd=' + ebRand + '"></scr' + 'ipt>');--}}
{{--</script>--}}
{{--<noscript>--}}
    {{--<img width="1" height="1" style="border:0"--}}
         {{--src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=900695&amp;ns=1"/>--}}
{{--</noscript>--}}

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
{{--<script type='text/javascript'>--}}
    {{--var ebRand = Math.random() + '';--}}
    {{--ebRand = ebRand * 1000000;--}}
    {{--document.write('<scr' + 'ipt src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=675540&amp;rnd=' + ebRand + '"></scr' + 'ipt>');--}}
{{--</script>--}}
{{--<noscript>--}}
    {{--<img width="1" height="1" style="border:0"--}}
         {{--src="HTTP://bs.serving-sys.com/Serving/ActivityServer.bs?cn=as&amp;ActivityID=675540&amp;ns=1"/>--}}
{{--</noscript>--}}

<div class="menu" id="top">
    <nav>
        <div class="ext-pad">
            <div class="container">
                <a href="#" class="hidden-sm js-toggle-menu"
                   onclick="$('.menu .mobile-menu').toggleClass('open'); return false;"><span
                            class="icon-toggle"></span></a>
                <a href="https://www.pepperidgefarm.com/product-finder?category=2368,2365,2364,2366,2363,2367">Find & Buy</a>
                <a href="#gallery">Gallery</a>
                <a href="#how-to-enter">How to enter</a>
                {{--<a href="{{ url(LaravelLocalization::getCurrentLocale()=='es' ? "" : "/es") }}" class="lang">{{ trans('index.language_switch') }}</a>--}}
                <div class="share">
                    <span class="icon-share" onclick="$(this).toggleClass('clicked');"></span>

                    <div id="social-popout">
                        <span class="icon-share-active" onClick="$('.icon-share').toggleClass('clicked');"></span>
                        <a class="share-facebook" target="_blank" ng-href="<% facebook %>"><span class="icon-facebook"></span></a>
                        <a class="share-twitter" target="_blank" ng-href="<% twitter %>"><span class="icon-twitter icon-twitter-brand"></span></a>
                    </div>
                </div>
                <a href="{{ LaravelLocalization::getCurrentLocale() }}/rules"
                   ng-click="openRules($event)">Rules</a>
                <a href="#prizes">Prizes</a>

                <div class="double-enclosure">
                    <div class="logo-wrapper">
                        <a href="#top" ><img src="{{ url('./img/logo.png') }}" alt="logo"></a>
                        {{--<div class="goldfish-logo"></div>--}}
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <div class="mobile-menu hidden-sm">
        <a href="#gallery">Gallery</a>
        <a href="#how-to-enter">How to enter</a>
        <a href="#prizes">Prizes</a>
        <a href="{{ LaravelLocalization::getCurrentLocale() }}/rules" ng-click="openRules($event)">Rules</a>
    </div>

    @yield('content')

    <footer>
        <div class="container tac">
            <div class="follow">
                <strong>{{ trans('index.footer_follow_us') }}</strong>
                <a target="_blank" href="https://www.instagram.com/pepperidgefarm/"><span class="icon-instagram"></span></a>
                <a target="_blank" href="https://twitter.com/pepperidgefarm/"><span class="icon-twitter"></span></a>
                <a target="_blank" href="https://www.facebook.com/PepperidgeFarm/"><span class="icon-facebook"></span></a>
                {{--<a target="_blank" href="https://youtube.com/goldfishsmiles"><span class="icon-youtube"></span></a>--}}
            </div>
            <div class="copyright">
                <p class="hidden-mobile">&copy;2018 Pepperidge Farm, Incorporated.<br/> All Rights Reserved.</p>
                <p class="hidden-sm">&copy;2018 Pepperidge Farm, Incorporated. All Rights Reserved.</p>
            </div>
            <div class="footer-menu">
                <p>
                    <a href="{{ route('rules') }}" class="rules-popup" ng-click="openRules($event)">Official Rules</a> |
                    <a href="https://www.campbellsoupcompany.com/privacy-policy/" target="_blank">Privacy</a> |
                    <a href="https://www.campbellsoupcompany.com/terms-of-use/" target="_blank">Terms of Use</a> |
                    <a href="http://www.pepperidgefarm.com/contact-us/" target="_blank">Contact Us</a>
                </p>
            </div>
        </div>
    </footer>
</div>

<script src="js/app.js"></script>

{{--<img src="http://linqia.ooh.li/c/0804f1e9af21a72a.png"/>--}}
{{--<script src="http://linqia.ooh.li/c/0804f1e9af21a72a.js"></script>--}}


@if(app()->environment() == 'production')
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-97360129-1', 'auto');
        ga('send', 'pageview');
    </script>
@endif
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '996599403810733',
            xfbml      : true,
            version    : 'v2.9'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>

</body>
</html>