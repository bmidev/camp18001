<?php namespace Brandmovers\Contests;

class PostValidator
{
	
	protected $rules = [];

	// Constructor
	public function __construct() {

	}

	public function addRule($rule) {
		$this->rules[] = $rule;
	}

	public function addRules($rules) {
		$this->rules = array_merge($this->rules, $rules);
	}

	public function isValid($post) {
		foreach ($this->rules as $rule) {
			if (preg_match($rule, $post) == 1) return TRUE;
		}
		return FALSE;
	}
}

